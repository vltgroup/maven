#!/bin/bash
set -eo pipefail
# This script will build and push docker image into AWS ECS repository.
# Arguments: docker-aws-push.sh image/name tag1 tag2 tag3 ...

# The following environment variables are supported:
# - AWS_ACCESS_KEY_ID
# - AWS_SECRET_ACCESS_KEY
# - AWS_ACCOUNT_ID
# - AWS_DEFAULT_REGION
# - BITBUCKET_BUILD_NUMBER
# - BITBUCKET_BRANCH
# - BITBUCKET_COMMIT
# - LABELS (optional)

IMAGE=$1

if [ -z "$IMAGE" ]; then
  echo "ERROR: Image name must be provided as the argument!"
  exit 1
fi

if [ -z "$AWS_ACCOUNT_ID" ]; then
  echo "ERROR: AWS_ACCOUNT_ID is not provided!"
  exit 1
fi

TAGS=${@:2}

# Detect version
if [ ! -z "$BITBUCKET_BRANCH" ] && \
   [ ! -z "$BITBUCKET_BUILD_NUMBER" ] && \
   [ ! -z "$BITBUCKET_COMMIT" ]; then
  BRANCH=${BITBUCKET_BRANCH/\//-}  # Replace / with -
  REV="${BRANCH:-master}-$BITBUCKET_BUILD_NUMBER"
  echo "Detected revision $REV"
  TAGS="$TAGS $REV $BRANCH"
fi

# Auto append commit label if BITBUCKET_COMMIT is defined
if [ ! -z "$BITBUCKET_COMMIT" ]; then
  LABELS="$LABELS commit=$BITBUCKET_COMMIT"
fi

# Auto append version label if $REV is defined
if [ ! -z "$REV" ]; then
  LABELS="$LABELS version=$REV"
fi

# Concat all labels
for label in ${LABELS}
do
  ADDLABELS="${ADDLABELS} --label $label"
done

if [ ! -z "$ADDLABELS" ]; then
  echo "The image will be labeled with: $LABELS"
fi

# Docker repository URL
REPO=$AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com

echo "Logging in repository: $REPO"

# Upload into AWS docker repository
aws ecr get-login-password | docker login --username AWS --password-stdin $REPO/$IMAGE

docker build -t "$REPO/$IMAGE:latest" ${ADDLABELS} .
docker push "$REPO/$IMAGE:latest"

# Process all the tags
for tag in $TAGS
do
  docker tag "$REPO/$IMAGE:latest" "$REPO/$IMAGE:$tag"
  docker push "$REPO/$IMAGE:$tag"
done
