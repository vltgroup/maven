FROM maven:3-jdk-8

RUN apt-get update \
    && apt-get install --no-install-recommends --no-install-suggests -y \
        python3-pip python3-setuptools \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Add docker-aws-push helper
COPY scripts/* /usr/local/bin/

RUN pip3 install awscli
